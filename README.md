# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database. This project was completed bt Anne Glickenhaus aglicken@uoregon.edu. All whiteboard logic was discussed over zoom with Alyssa Kelley.

## What is in this repository

To run this program the command " docker-compose up --build " will build the docker comtainer and link it to the mongo image to use for database storage. 
Once the compose has finished building you will be able to follow the link to the locally hosted webpage.

## Testing

To test improper use functionality is working for both "Submit" and "Display", first click "Submit" to see that you are redirected to an error page stating you must enter times before submission. Then redirect back to the index page and click "Display" to see that you are redirected to an error page stating you must enter times before they can be displed. 
To test proper use functionality is working for both "Submit" and "Display", enter times into the "Miles" column ensuring the miles are entered as whole integers then click "Submit" which will redirect you back to the /index route with a cleared index page. Then click "Display" to see checkpoints with correspoding opening and closing times are displayed.

The database items are only stored during a current session. When the container is stopped in the terminal, all database items are erased. 

